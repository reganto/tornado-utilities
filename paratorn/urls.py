from handlers.foo import FooHandler
from handlers.get_parameter import GetParameter
from handlers.other_request import OtherRequest

url_patterns = [
    (r"/get/parameter", GetParameter),
    (r"/other/request", OtherRequest),
    (r"/", FooHandler),
]
