Send parameter to other request with POST method in Tornado 
===============================================================================

## Description

[Tornado](http://www.tornadoweb.org/) send parameters to other request with GET method
(query string).

I use a trick to send a parameter to other request with POST method :)

### Related Projects

[tornado](https://github.com/reganto/tornado)

[tornado-boilerplate](https://github.com/reganto/tornado-boilerplate)

### Requirements

```sh
    pip install tornado
```

#### Start

```sh
    python app.py --port=8888
```

In your browser go to `localhost:8888`

## Contributing

If you have improvements or bug fixes:

* Fork the repository on GitHub
* File an issue for the bug fix/feature request in GitHub
* Create a topic branch
* Push your modifications to that branch
* Send a pull request

## Author

* [Reganto Blog](http://www.reganto.blog.ir)
* [Reganto Github](https://github.com/reganto)