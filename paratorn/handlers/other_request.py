from handlers.base import BaseHandler


class OtherRequest(BaseHandler):
    def post(self):
        parameter = self.get_body_argument('parameter')

        self.write({'parameter':parameter})
