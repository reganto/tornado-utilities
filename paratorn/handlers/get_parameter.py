from handlers.base import BaseHandler


class GetParameter(BaseHandler):
    def get(self):
        self.render('first.html')

    def post(self):
        parameter = self.get_body_argument('parameter')

        self.render('hide.html', parameter=parameter)
