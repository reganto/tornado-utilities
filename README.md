# Utilities for [Tornado](https://gitlab.com/reganto/tornado)

## Utilities

- Check Captcha
    - A utility to work with Google captcha
- Email validation
    - A utility to check email validation with *neverbounce* api
- Paratorn
    - Send parameter(s) to other request without using of sessions
- Send Verification Email
    - A utility to send email
- Tornado Boilerplate
    - A template for Tornado projects
