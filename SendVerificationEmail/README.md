Send Verification Email (SMTP) utility for web applications
==============================

## Related projects
[tornado](https://githla.com/reganto/tornado)

## Utility

* `Send Verification Email`

## Usage

set `sender` and `password` in send_verification_email function

change url to handle requests

```bash
git clone https://gitlab.com/reganto/SendVerificationEmail
```

then import utility to your project

## Contributing

If you have improvements or bug fixes:

* Fork the repository on GitHub
* File an issue for the bug fix/feature request in GitHub
* Create a topic branch
* Push your modifications to that branch
* Send a pull request

## Authors

* [Reganto Blog](http://reganto.blog.ir)
* [Reganto Gitlab](https://gitlab.com/reganto)
